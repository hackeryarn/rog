# ROG

A static site generator that makes it simple to customize the rendering and
organization of content.

## Core Concepts

- Router that accepts a path, template, and render function.
- Templates created in raw s-expressions to provide a full power of Racket.
- Includes renderers for markdown.

### Router

The router handles the generation of the files in accordance to the passed in
parameters.

#### Path

``` racket
path?
```

A route can take a single file or a directory.

If the router receives a single file, it will make the file name, without the
file extension, the route.

If the router receives a directory, it will generate a route for every file in
the directory. The route consists of the directory path plus the file name,
without the file extension. 

#### Template

``` racket
(listof xexpr?) -> (listof xexpr?)
```

A template is a Racket function that accepts a single argument of `content` 
and produces an `(listof xexpr?)` representing the HTML content to render.

The implementation should simply wrap the content in a generic template.

#### Renderer

``` racket
(or/c path? string?) -> (listof xexpr?)
```

The renderer is a Racket function that accepts a `path` to a file with content
or a `string` of the content. It transforms the content into a `(listof xexpr?)`
that represents the HTML content to render.

The implementation should support the specific markup format provided. To help
identify the supported content the name of renderers should start with the word
`render` followed by the file content type. (e.g. `render-markdown-post`)
